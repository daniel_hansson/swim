//
//  GameScene.swift
//  Swim
//
//  Created by Daniel Hansson on 2016-04-10.
//  Copyright (c) 2016 Daniel Hansson. All rights reserved.
//

import SpriteKit
import AVFoundation


class GameScene: SKScene {
    
    override func didMoveToView(view: SKView) {
        Music.sharedInstance.startMusic()
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let touch = touches.first!
        let startLabel = childNodeWithName("StartLabel")
        let loadLabel = childNodeWithName("LoadLabel")
        if startLabel!.containsPoint(touch.locationInNode(self)) {
            startGame(1, player: nil)
        }
        if loadLabel!.containsPoint(touch.locationInNode(self)) {
            loadSavedGame()
        }
    }
    
    func loadSavedGame(){
        print("LOADING")
        if let savedGame: Int = NSUserDefaults.standardUserDefaults().objectForKey("SavedGame") as? Int {
            let life: Int = (NSUserDefaults.standardUserDefaults().objectForKey("Life") as? Int)!
            let health: Int = (NSUserDefaults.standardUserDefaults().objectForKey("Health") as? Int)!
            let maxSpeed : Float = (NSUserDefaults.standardUserDefaults().objectForKey("MaxSpeed") as? Float)!
            let player = createPlayerFromLoadedData(life, health: health, maxSpeed: maxSpeed)
            print("Loading level: \(savedGame) L: \(life) HP: \(health)")
            startGame(savedGame, player: player)
        }else {
            let label = childNodeWithName("LoadLabel") as! SKLabelNode
            label.text = "No game to load..."
        }
        
    }
    
    func createPlayerFromLoadedData(life: Int, health: Int, maxSpeed: Float)-> GameObject{
        let sprite = SKSpriteNode(imageNamed: "boy_1")
        let player = GameObject(sprite: sprite, position: CGPointMake(0.0, 0.0) , speed: 3)
        player.controllerComponent = PlayerController(player: player)
        player.damageableComponent = PlayerController(player: player)
        player.health = health
        player.life = life
        player.maxSpeed = maxSpeed
        return player
    }
    
    func startGame(level: Int, player: GameObject?){
        let transferFade = SKAction.fadeInWithDuration(0.5)
        self.runAction(transferFade, completion: {
            let transition = SKTransition.doorsOpenHorizontalWithDuration(0.5)
            let swimScene = SwimScene(fileNamed: "Level\(level)")
            swimScene?.currentLevel = level
            if let player = player {
                swimScene?.player = player
            }
            self.view?.presentScene(swimScene!, transition: transition)
        })
    }
   
    override func update(currentTime: CFTimeInterval) {
        
    }
}
