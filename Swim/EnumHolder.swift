//
//  EnumHolder.swift
//  Swim
//
//  Created by Daniel Hansson on 2016-04-14.
//  Copyright © 2016 Daniel Hansson. All rights reserved.
//

import Foundation


struct PhysicsCategory {
    // Collide Categories
    static let playerCategory : UInt32 = 0b1 //  1
    static let mobCategory : UInt32 = 0b10 //  2
    static let terrainCategory : UInt32 = 0b100 //  4
    static let dangerusTerrainCategory : UInt32 = 0b1000 //  8
    static let goalCategory : UInt32 = 0b10000 // 16
    static let wallCategory : UInt32 = 0b100000 // 32
}

struct SoundEffects {
    
}
