//
//  SwimScene.swift
//  Swim
//
//  Created by Daniel Hansson on 2016-04-13.
//  Copyright © 2016 Daniel Hansson. All rights reserved.
//

import UIKit
import SpriteKit

let loseSound = SKAction.playSoundFileNamed("lose", waitForCompletion: true)


class SwimScene: SKScene, SKPhysicsContactDelegate {
    var player : GameObject?
    var goal : GameObject?
    var mobList : [GameObject] = []
    var terrainList : [GameObject] = []
    
    var lastTouch: CGPoint? = nil
    var startPosition : CGPoint?
    var currentLevel : Int = 0
    var hud : SKSpriteNode?
    var hudHandler : Hud?
    var controllerPad : SKSpriteNode?
    
    override func didMoveToView(view: SKView) {
        self.physicsWorld.contactDelegate = self
        
        for child in children {
            if (child.name == "Player") {
                setUpPlayer(child.position)
                startPosition = child.position
            }
            if (child.name == "Tentacle"){
                setUpMobs(child.position, sprite: "tentacle")
            }
            if (child.name == "Shark"){
                setUpMobs(child.position, sprite: "shark")
            }
            if (child.name == "NormalTerrain"){
                setUpTerrain(child.position, sprite: "rock", dangerus: false)
            }
            if (child.name == "DangerusTerrain") {
                setUpTerrain(child.position, sprite: "dangerusrock", dangerus: true)
            }
            if (child.name == "Wall"){
                setWalls(child)
            }
            if (child.name == "Goal") {
                setGoal(child.position)
            }
        }
        
        setHud()
        // Future setup for controllPad
        //setControlerPad()
    }
    
    func setControlerPad() {
        if let camera = camera {
            controllerPad = SKSpriteNode(color: UIColor.blackColor(), size: CGSizeMake(128, 128))
            controllerPad?.zPosition = 6
            let cPW = (controllerPad?.size.width)!/2
            let Y = (self.frame.size.height/2)-cPW
            let X = (self.frame.size.width/2)-cPW
            controllerPad?.position = CGPointMake(-X, -Y)
            camera.addChild(controllerPad!)
        }
    }
    
    func setHud() {
        if let camera = camera {
            hudHandler = Hud(hud: SKSpriteNode(color: UIColor.grayColor(), size: CGSizeMake(self.frame.width-32, 32)))
            hud = hudHandler!.getHud()
            let height = self.frame.size.height/2
            let margin = self.frame.size.height/10
            let y = height-margin
            hud?.zPosition = 9
            hud!.position = CGPoint(x: 0.0, y: y)
            camera.addChild(hud!)
            // Level Label
            let levelLabel = SKLabelNode(text: "Level \(currentLevel). Find the island!")
            camera.addChild(levelLabel)
            let wait = SKAction.waitForDuration(1)
            let remove = SKAction.runBlock({levelLabel.removeFromParent()})
            let fade = SKAction.fadeOutWithDuration(1)
            let waitRemove = SKAction.sequence([wait, fade, remove])
            levelLabel.runAction(waitRemove)
        }
    }
    
    func updatHud() {
        hudHandler?.updateHud((player?.life)!, nHealth: (player?.health)!, nTentacles: (player?.attatched)!)
    }
    
    func updateCamera() {
        if let camera = camera {
            camera.position = CGPoint(x: player!.sprite.position.x, y: player!.sprite.position.y)
        }
    }
    
    func setUpPlayer(pos: CGPoint) {
        // Sprite setup
        let playerSprite = SKSpriteNode(imageNamed: "boy_0")
        playerSprite.position = pos
        playerSprite.xScale = 1
        playerSprite.yScale = 1
        
        // Physics setup
        playerSprite.physicsBody = SKPhysicsBody(circleOfRadius: playerSprite.size.width/2)
        playerSprite.physicsBody?.dynamic = true
        playerSprite.physicsBody?.affectedByGravity = false
        playerSprite.physicsBody?.categoryBitMask = PhysicsCategory.playerCategory
        playerSprite.physicsBody?.contactTestBitMask = PhysicsCategory.mobCategory
        playerSprite.physicsBody?.collisionBitMask =
            PhysicsCategory.mobCategory |
            PhysicsCategory.terrainCategory |
            PhysicsCategory.dangerusTerrainCategory |
            PhysicsCategory.goalCategory |
            PhysicsCategory.wallCategory
        self.addChild(playerSprite)
        setPlayerSpriteToPlayer(playerSprite)
    }
    
    func setPlayerSpriteToPlayer(playerSprite : SKSpriteNode){
        // GameObject setup
        if (player == nil) {
            player = GameObject(sprite: playerSprite, position: playerSprite.position, speed: 1.0)
            player?.controllerComponent = PlayerController(player: player!)
            player?.damageableComponent = PlayerController(player: player!)
            player?.health = 6
            player?.life = 3
            player?.maxSpeed = 1.0
        }else {
            player?.sprite = playerSprite
            player?.setAnimation("boy")
            player!.sprite.runAction((player?.animation)!)
            player?.speed = (player?.maxSpeed)!
        }
        player?.startPosition = player?.sprite.position
        player?.bleeding = false
        player?.attatched = 0
        player?.moving = false
        player?.setAnimation("boy")
        player!.sprite.runAction((player?.animation)!)
    }
    
    func setUpMobs(pos: CGPoint, sprite: String){
        // Sprite setup
        let mobSprite = SKSpriteNode(imageNamed: "\(sprite)_0")
        setUpMobs(pos, mobSprite: mobSprite, name: sprite)
    }
    
    func setUpMobs(pos: CGPoint, mobSprite: SKSpriteNode, name: String){
        mobSprite.position = pos
        mobSprite.xScale = 1
        mobSprite.yScale = 1
        // Physics setup
        mobSprite.physicsBody = SKPhysicsBody(rectangleOfSize: mobSprite.size)
        mobSprite.physicsBody?.dynamic = true
        mobSprite.physicsBody?.affectedByGravity = false
        mobSprite.physicsBody?.categoryBitMask = PhysicsCategory.mobCategory
        mobSprite.physicsBody?.contactTestBitMask = PhysicsCategory.mobCategory | PhysicsCategory.playerCategory
        mobSprite.physicsBody?.collisionBitMask =
            PhysicsCategory.mobCategory |
            PhysicsCategory.playerCategory |
            PhysicsCategory.terrainCategory |
            PhysicsCategory.dangerusTerrainCategory |
            PhysicsCategory.goalCategory
        self.addChild(mobSprite)
        
        // GameObject setup
        let speed: Float = 0.8
        let mob = GameObject(sprite: mobSprite, position: mobSprite.position, speed: speed)
        if name == "shark" {
            mob.hunterComponent = MobBehavior(mob: mob)
            mob.hurtfullComponent = DamageHandler(attacker: mob)
        }
        if name == "tentacle" {
            mob.hunterComponent = MobBehavior(mob: mob)
            mob.attatchComponent = MobAttatch(mob: mob)
        }
        mob.setAnimation(name)
        mob.sprite.runAction(mob.animation!)
        mobList.append(mob)
    }

    func setUpTerrain(pos: CGPoint, sprite: String, dangerus : Bool) {
        // Sprite setup
        let randomImg = 3 + arc4random() % 3
        let rockImg = sprite + String(randomImg)
        let terrainSprite = SKSpriteNode(imageNamed: rockImg)
        terrainSprite.position = pos
        terrainSprite.xScale = 1
        terrainSprite.yScale = 1
        
        if (arc4random() % 2) == 1 {
            let rotate = SKAction.rotateToAngle(CGFloat(M_PI/2), duration: 0.0)
            terrainSprite.runAction(rotate)
        }
        
        let W = terrainSprite.size.width-16
        let H = terrainSprite.size.height-16
        // Physics setup
        terrainSprite.physicsBody = SKPhysicsBody(rectangleOfSize: CGSize(width: W, height: H))
        terrainSprite.physicsBody?.dynamic = false
        terrainSprite.physicsBody?.affectedByGravity = false
        terrainSprite.physicsBody?.contactTestBitMask = PhysicsCategory.mobCategory | PhysicsCategory.playerCategory
        terrainSprite.physicsBody?.collisionBitMask = PhysicsCategory.mobCategory | PhysicsCategory.playerCategory
        // GameObject setup
        self.addChild(terrainSprite)
        let terrain = GameObject(sprite: terrainSprite, position: terrainSprite.position, speed: 0.0)
        if dangerus {
            terrain.dangerusTerrainComponent = TerrainBehavior(terrain: terrain)
            terrainSprite.physicsBody?.categoryBitMask = PhysicsCategory.dangerusTerrainCategory
        }else {
            terrainSprite.physicsBody?.categoryBitMask = PhysicsCategory.terrainCategory
        }
        terrainList.append(terrain)
    }
    
    func setWalls(wall : SKNode) {
        wall.physicsBody = SKPhysicsBody(rectangleOfSize: wall.frame.size)
        wall.physicsBody?.dynamic = false
        wall.physicsBody?.affectedByGravity = false
        wall.physicsBody?.categoryBitMask = PhysicsCategory.wallCategory
        wall.physicsBody?.contactTestBitMask = PhysicsCategory.mobCategory | PhysicsCategory.playerCategory
        wall.physicsBody?.collisionBitMask = PhysicsCategory.mobCategory | PhysicsCategory.playerCategory
    }
    
    func setGoal(pos: CGPoint){
        let goalSprite = SKSpriteNode(imageNamed: "goal")
        goalSprite.position = pos
        self.addChild(goalSprite)
        let W = goalSprite.size.width
        // Physics setup
        goalSprite.physicsBody = SKPhysicsBody(circleOfRadius: W/2)
        goalSprite.physicsBody?.dynamic = false
        goalSprite.physicsBody?.affectedByGravity = false
        goalSprite.physicsBody?.categoryBitMask = PhysicsCategory.goalCategory
        goalSprite.physicsBody?.contactTestBitMask = PhysicsCategory.mobCategory | PhysicsCategory.playerCategory
        goalSprite.physicsBody?.collisionBitMask = PhysicsCategory.mobCategory | PhysicsCategory.playerCategory
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let location = touches.first!.locationInNode(self)
        lastTouch = location
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let location = touches.first!.locationInNode(self)
        lastTouch = location
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        lastTouch = player?.sprite.position
    }
    
    override func didSimulatePhysics() {
        updateCamera()
    }
    
    var lastT : Double = 0
    override func update(currentTime: CFTimeInterval) {
        var deltaTime = currentTime - lastT
        if(deltaTime > 1.0) {
            deltaTime = 1.0
        }
        if (lastTouch != nil) {
            player?.tick(Float(deltaTime), lastTouch: lastTouch!)
        }
        var range: CGFloat = 100
        if player!.bleeding {
            range = 144
        }
        for mob in mobList {
            mob.hunterComponent?.hunt((player?.sprite.position)!, deltaTime: Float(deltaTime), range: range)
        }
        
        updatHud()
        
        if (player!.damageableComponent!.isDead()) {
            gameOver(didWin: false)
        }
    }
    
    func getOwner(body : SKPhysicsBody) -> GameObject? {
        for go in mobList {
            if go.sprite.physicsBody == body {
                return go
            }
        }
        for go in terrainList {
            if go.sprite.physicsBody == body {
                return go
            }
        }
        return nil
    }
    
    func didBeginContact(contact: SKPhysicsContact) {
        var secondBody: SKPhysicsBody
        
        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask {
            secondBody = contact.bodyB
        } else {
            secondBody = contact.bodyA
        }
        let collideBitMask = contact.bodyA.categoryBitMask | contact.bodyB.categoryBitMask
        
        switch collideBitMask {
        //Player - Mob
        case PhysicsCategory.playerCategory | PhysicsCategory.mobCategory:
            if let owner = getOwner(secondBody) {
                owner.hurtfullComponent?.giveDamage(player!)
                owner.attatchComponent?.attatch(player!)
            }
        // Player - Terrain
        case PhysicsCategory.playerCategory | PhysicsCategory.terrainCategory:
            print("Player - Terrain contact")
        case PhysicsCategory.playerCategory | PhysicsCategory.dangerusTerrainCategory:
            if let owner = getOwner(secondBody) {
                owner.dangerusTerrainComponent?.makeBleed(player!)
            }
        // Player - Goal
        case PhysicsCategory.playerCategory | PhysicsCategory.goalCategory:
            gameOver(didWin: true)
        default:
            print("Defaut contact")
        }
    }
    
    func didEndContact(contact: SKPhysicsContact) {
    }
    
    func gameOver(didWin didWin : Bool){
        Music.sharedInstance.stopMusic()
        //didWin ? print("Next Level") : print("You Died")
        if didWin {
            let transition = SKTransition.fadeWithColor(UIColor.blueColor(), duration: 0.7)
            let nextLevel = NextLevel(fileNamed: "NextLevel")
            nextLevel?.scaleMode = SKSceneScaleMode.AspectFill
            nextLevel?.player = player
            nextLevel?.nextLevel = currentLevel + 1
            self.view?.presentScene(nextLevel!, transition: transition)
        }else {
            runAction(loseSound)
            let transition = SKTransition.fadeWithColor(UIColor.redColor(), duration: 1.2)
            let gameScene = GameScene(fileNamed: "GameScene")
            gameScene!.scaleMode = SKSceneScaleMode.AspectFill
            self.view?.presentScene(gameScene!, transition: transition)
        }
    }
    
    func movePlayerToStartPosition(){
        let trans = SKAction.fadeOutWithDuration(1.0)
        let move = SKAction.moveTo(startPosition!, duration: 0.0)
        let actionSet = SKAction.sequence([trans, move])
        player!.sprite.runAction(actionSet)
    }

}
