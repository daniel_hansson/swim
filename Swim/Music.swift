//
//  Music.swift
//  Swim
//
//  Created by Daniel Hansson on 2016-05-01.
//  Copyright © 2016 Daniel Hansson. All rights reserved.
//

import Foundation
import AVFoundation

public class Music {
    static let sharedInstance: Music = Music()
    var backgroundMusicPlayer: AVAudioPlayer?
    private init() {
        
    }
    func startMusic(){
        do {
            let url = NSBundle.mainBundle().URLForResource("song", withExtension: "wav")
            backgroundMusicPlayer = try AVAudioPlayer(contentsOfURL: url!)
        }catch{
            
        }
        backgroundMusicPlayer?.numberOfLoops = -1
        backgroundMusicPlayer?.volume = 0.2
        backgroundMusicPlayer?.play()
    }
    func stopMusic(){
        backgroundMusicPlayer?.stop()
    }
}
