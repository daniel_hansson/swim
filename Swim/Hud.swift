//
//  Hud.swift
//  Swim
//
//  Created by Daniel Hansson on 2016-04-20.
//  Copyright © 2016 Daniel Hansson. All rights reserved.
//

import Foundation

import SpriteKit


class Hud {
    var life : Int = 0
    var health : Int = 0
    var tentacles : Int = 0
    var hud : SKSpriteNode
    var l1 :SKSpriteNode?
    var l2 :SKSpriteNode?
    var l3 :SKSpriteNode?
    var healthBar : SKSpriteNode?
    var attatched : SKSpriteNode?
    
    init(hud: SKSpriteNode){
        self.hud = hud
        setup()
    }
    func setup(){
        let leftSide = (hud.size.width/2)
        hud.color = UIColor.init(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.1)
        l1 = SKSpriteNode(imageNamed: "lives_1")
        l1!.position = CGPoint(x: -leftSide+16, y: 0.0)
        l2 = SKSpriteNode(imageNamed: "lives_1")
        l2!.position = CGPoint(x: -leftSide+48, y: 0.0)
        l3 = SKSpriteNode(imageNamed: "lives_1")
        l3!.position = CGPoint(x: -leftSide+80, y: 0.0)
        hud.addChild(l1!)
        hud.addChild(l2!)
        hud.addChild(l3!)
        healthBar = SKSpriteNode(imageNamed: "Healthbar3")
        healthBar?.position = CGPoint(x: 0.0, y: 0.0)
        hud.addChild(healthBar!)
        attatched = SKSpriteNode(imageNamed: "attT_0")
        attatched?.position = CGPoint(x: leftSide/1.5, y: 0.0)
        hud.addChild(attatched!)
    }
    func getHud() -> SKSpriteNode {
        return hud
    }
    
    func updateHud(nLife: Int, nHealth: Int, nTentacles: Int){
        if !(nLife==life){
            if nLife==1 {
                l1?.texture = SKTexture(imageNamed: "lives_1")
                l2?.texture = SKTexture(imageNamed: "lives_2")
                l3?.texture = SKTexture(imageNamed: "lives_2")
            }else if nLife==2{
                l1?.texture = SKTexture(imageNamed: "lives_1")
                l2?.texture = SKTexture(imageNamed: "lives_1")
                l3?.texture = SKTexture(imageNamed: "lives_2")
            }else if nLife==3{
                l1?.texture = SKTexture(imageNamed: "lives_1")
                l2?.texture = SKTexture(imageNamed: "lives_1")
                l3?.texture = SKTexture(imageNamed: "lives_1")
            }
            life = nLife
        }
        if !(nHealth==health){
            let healthImg = "Healthbar\(nHealth)"
            healthBar?.texture = SKTexture(imageNamed: healthImg)
            health=nHealth
        }
        if !(tentacles==nTentacles) {
            let tentaclesImg = "attT_\(nTentacles)"
            attatched?.texture = SKTexture(imageNamed: tentaclesImg)
            tentacles=nTentacles
        }
    }
    
}
