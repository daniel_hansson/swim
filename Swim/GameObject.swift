//
//  GameObject.swift
//  Swim
//
//  Created by Daniel Hansson on 2016-04-12.
//  Copyright © 2016 Daniel Hansson. All rights reserved.
//

import Foundation
import SpriteKit

class GameObject {
    // Commonly
    var sprite : SKSpriteNode
    let position : CGPoint
    var animation : SKAction?
    var speed : Float
    var bleeding : Bool
    var bleedTime : Int
    var moving : Bool
    // Player
    var health : Int?
    var life : Int?
    var attatched : Int?
    var maxSpeed : Float?
    var startPosition : CGPoint?
    var controllerComponent : Controllable?
    var damageableComponent : Damageable?
    // Mob
    var hunterComponent : Hunter?
    var hurtfullComponent : Hurtfull?
    var attatchComponent : Attachable?
    var hunting : Bool = false
    // Terrain
    var dangerusTerrainComponent : DangerusTerrain?
    var normalTerrain : Terrain?
    
    init(sprite : SKSpriteNode, position : CGPoint, speed: Float) {
        self.sprite = sprite
        self.position = position
        self.speed = speed
        self.moving = false
        self.bleeding = false
        self.bleedTime = 6
    }
    
    
    func tick(deltaTime : Float, lastTouch: CGPoint) {
        controllerComponent?.moveToPoint(lastTouch, deltaTime: deltaTime)
    }
    
    func setAnimation(name: String){
        let atlas = SKTextureAtlas(named: name)
        
        let anim = SKAction.animateWithTextures([
            atlas.textureNamed("\(name)_0"),
            atlas.textureNamed("\(name)_1"),
            atlas.textureNamed("\(name)_2"),
            atlas.textureNamed("\(name)_3"),
            atlas.textureNamed("\(name)_4"),
            atlas.textureNamed("\(name)_5")
            ], timePerFrame: 0.2 )
        self.animation = SKAction.repeatActionForever(anim)
        
    }
}

class PlayerController : Controllable, Damageable {
    let player : GameObject
    
    init(player : GameObject) {
        self.player = player
    }
    
    // Controlled by buttons
    func control(direction: Int, deltaTime : Float){
        
        let angle: Float
        switch direction {
        case 1:
            // LEFT
            angle = Float(M_PI)
        case 2:
            // UP
            angle = Float(M_PI/2)
        case 3:
            // RIGHT
            angle = Float(M_PI*2)
        case 4:
            // DOWN
            angle = Float(M_PI*1.5)
        default:
            print("Something went wrong")
            angle = Float(M_PI)
        }
        
        let newX = Float(player.sprite.position.x) + cosf(angle) * player.speed * deltaTime // 2:an e speed
        let newY = Float(player.sprite.position.y) + sinf(angle) * player.speed * deltaTime
        let rotate = SKAction.rotateToAngle(CGFloat(angle) + CGFloat(M_PI*1.5), duration: 0.0)
        player.sprite.runAction(rotate)
        player.sprite.position = CGPoint(x: Double(newX), y: Double(newY))
    }
    
    // Controlled by touche
    func moveToPoint(point: CGPoint, deltaTime: Float) {
        if isIdle(currentPosition: player.sprite.position, destinationPosition: point) {
            let rotate = SKAction.rotateToAngle(CGFloat(M_PI*2), duration: 0.0)
            player.sprite.runAction(rotate)
            player.sprite.physicsBody?.resting = true
            //  Change animation
            if player.moving {
                player.setAnimation("boy")
                player.sprite.runAction(player.animation!)
                player.moving = false
            }
        }else {
            let angle: Float = getAngle(player.sprite.position, point2: point)
            let newX = Float(player.sprite.position.x) + cosf(angle) * player.speed * deltaTime
            let newY = Float(player.sprite.position.y) + sinf(angle) * player.speed * deltaTime
            let rotate = SKAction.rotateToAngle(CGFloat(angle) + CGFloat(M_PI*1.5), duration: 0.0)
            player.sprite.runAction(rotate)
            player.sprite.position = CGPoint(x: Double(newX), y: Double(newY))
            // Change animation
            if !(player.moving) {
                player.setAnimation("swiming")
                player.sprite.runAction(player.animation!)
                player.moving = true
            }
        }
    }
    
    func isIdle(currentPosition currentPosition: CGPoint, destinationPosition: CGPoint) -> Bool{
        return (getDistance(currentPosition, point2: destinationPosition)) < 3
    }
    
    func takeDamage() {
        player.health = player.health!-1
        if (player.health<=0) {
            player.damageableComponent?.loseLife()
        }
    }
    func loseLife() {
        // Move to start if life lost.
        /*
        let fadeOut = SKAction.fadeOutWithDuration(0.6)
        let fadeIn = SKAction.fadeInWithDuration(0.6)
        let move = SKAction.moveTo(player.startPosition!, duration: 0.0)
        let actionSet = SKAction.sequence([fadeOut, move, fadeIn])
        player.sprite.runAction(actionSet)
        */
        player.life = player.life!-1
        if player.life > 0 {
            player.health = 6
        }
    }
    func isDead() -> Bool {
        return player.life <= 0
    }
    
    func numberOfAttatched() -> Int {
        return player.attatched!
    }
    
    func startBleed(){
        player.bleeding = true
        let blood = SKEmitterNode(fileNamed: "Blood")
        blood?.targetNode = player.sprite.parent
        player.sprite.addChild(blood!)
        
        let bT = NSTimeInterval(player.bleedTime)
        
        let bleedTime = SKAction.waitForDuration(bT)
        let fadeTime = SKAction.waitForDuration(2)
        let b1 = SKAction.runBlock({let blood = self.player.sprite.childNodeWithName("blood") as! SKEmitterNode
            blood.particleBirthRate = 0})
        let b2 = SKAction.runBlock({blood?.removeFromParent()
            self.player.bleeding = false})
        let timedBleedSequence = SKAction.sequence([bleedTime, b1, fadeTime, b2])
        player.sprite.runAction(timedBleedSequence)
    }
}


class DamageHandler : Hurtfull{
    let attacker : GameObject
    
    init(attacker : GameObject) {
        self.attacker = attacker
    }
    func giveDamage(target : GameObject) {
        target.damageableComponent?.takeDamage()
        
    }
}

class MobAttatch: Attachable {
    let mob : GameObject
    init(mob : GameObject){
        self.mob = mob
    }
    func attatch(target: GameObject){
        let armSprite = SKSpriteNode(imageNamed: "attatched")
        armSprite.name = "armSprite"
        armSprite.position.x = CGFloat(arc4random() % 5)
        armSprite.position.y = -12
        target.sprite.addChild(armSprite)
        target.attatched = target.attatched!+1
        if target.attatched == 3{
            target.damageableComponent?.loseLife()
            target.attatched = 0
            target.speed = target.maxSpeed!
            for child in target.sprite.children {
                if child.name == "armSprite" {
                    child.removeFromParent()
                }
            }
        }
        target.speed = target.speed-0.1
        mob.sprite.removeFromParent()
    }
}

class MobBehavior : Hunter {
    let sharkSound = SKAction.playSoundFileNamed("shark", waitForCompletion: false)
    let mob : GameObject

    init(mob : GameObject) {
        self.mob = mob
    }
    func hunt(playerPos: CGPoint, deltaTime: Float, range: CGFloat) {
        if (getDistance(playerPos, point2: mob.sprite.position) < range) {
            if !mob.hunting{
                mob.sprite.runAction(sharkSound)
            }
            mob.hunting = true
            let angle: Float = getAngle(mob.sprite.position, point2: playerPos)
            
            let newX = Float(mob.sprite.position.x) + cosf(angle) * mob.speed * deltaTime // 2:an e speed
            let newY = Float(mob.sprite.position.y) + sinf(angle) * mob.speed * deltaTime
            let rotate = SKAction.rotateToAngle(CGFloat(angle) + CGFloat(M_PI*1.5), duration: 0.0)
            mob.sprite.runAction(rotate)
            mob.sprite.position = CGPoint(x: Double(newX), y: Double(newY))
            
        }else{
            mob.hunting = false
        }
    }
}

class TerrainBehavior : DangerusTerrain, Terrain {
    let terrain : GameObject
    
    init(terrain : GameObject){
        self.terrain = terrain
    }
    func makeBleed(target: GameObject) {
        if !target.bleeding {
            target.damageableComponent?.startBleed()
        }
    }
}

//  Help Functions

func getDistance(point1: CGPoint, point2 : CGPoint) -> CGFloat {
    let diffX = point1.x - point2.x;
    let diffY = point1.y - point2.y;
    let distance = sqrt(diffX * diffX + diffY * diffY)
    return distance
}

func getAngle(point1: CGPoint, point2 : CGPoint) ->  Float{
    let angle = atan2(point1.y - point2.y, point1.x - point2.x) + CGFloat(M_PI)
    return Float(angle)
}
