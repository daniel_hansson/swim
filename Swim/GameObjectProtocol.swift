//
//  GameObjectProtocol.swift
//  Swim
//
//  Created by Daniel Hansson on 2016-04-12.
//  Copyright © 2016 Daniel Hansson. All rights reserved.
//

import Foundation
import SpriteKit

// Playerstuff
protocol Controllable {
    func control(direction: Int, deltaTime : Float)
    func moveToPoint(point: CGPoint, deltaTime: Float)
}

protocol Damageable {
    func takeDamage()
    func startBleed()
    
    func loseLife()
    func isDead() -> Bool
    func numberOfAttatched() -> Int
}
// MobStuff
protocol  Hurtfull {
    func giveDamage(target : GameObject)
}

protocol Attachable {
    func attatch(target: GameObject)
}

protocol Hunter {
    func hunt(playerPos: CGPoint, deltaTime: Float, range : CGFloat)
}
// Terrain
protocol Terrain {
    
}

protocol DangerusTerrain {
    func makeBleed(target : GameObject)
}
