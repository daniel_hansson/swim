//
//  NextLevel.swift
//  Swim
//
//  Created by Daniel Hansson on 2016-04-22.
//  Copyright © 2016 Daniel Hansson. All rights reserved.
//

import Foundation
import SpriteKit

let winSound = SKAction.playSoundFileNamed("goal", waitForCompletion: true)


class NextLevel: SKScene {
    
    var player : GameObject?
    var nextLevel : Int = 3
    
    var powerupPicked : Bool = false
    var nextLabel : SKLabelNode?
    var messageLabel : SKLabelNode?
    var powerUp1 : SKSpriteNode?
    var p1: Int = 0
    var powerUp2 : SKSpriteNode?
    var p2: Int = 0
    var mainMenuLabel : SKNode?
    var saveLabel : SKNode?
    
    override func didMoveToView(view: SKView) {
        runAction(winSound)
        nextLabel = childNodeWithName("NextLabel") as? SKLabelNode
        messageLabel = childNodeWithName("MessageLabel") as? SKLabelNode
        powerUp1 = (childNodeWithName("Powerup1") as! SKSpriteNode)
        powerUp2 = (childNodeWithName("Powerup2") as! SKSpriteNode)
        mainMenuLabel = childNodeWithName("StartMenuLabel")
        saveLabel = childNodeWithName("SaveLabel")
        
        // EndGameFix
        if nextLevel == 5 {
            nextLevel = 1
        }
        
        messageLabel?.text = "Life: \(player!.life!)/3 HP: \(player!.health!)/6 Speed:\(player!.speed)"
        setPowerup()
    }
    
    func runPowerup(powerup: Int){
        var message = ""
        switch powerup {
        case 1:
            if player?.life<3 {
                player?.life! += 1
                message = "+ 1 life!"
            }else {
                messageLabel?.text = "You have full life!"
                return
            }
        case 2:
            player?.health = 6
            message = "Full HP!"
        case 3:
            player?.maxSpeed! += 0.1
            message = "Added speed!"
        case 4:
            player?.bleedTime -= 1
            message = "Reduced bleedtime!"
        default:
            print("")
        }
        powerUp1?.removeFromParent()
        powerUp2?.removeFromParent()
        messageLabel?.text = message
        nextLabel?.text = "To next level!"
        powerupPicked = true
    }
    
    func setPowerup(){
        var startValue = 1
        var stopValue: UInt32 = 4
        if player?.life == 3 {
            startValue = 2
            stopValue = 3
        }
        
        p1 = startValue + Int(arc4random() % stopValue)
        p2 = startValue + Int(arc4random() % stopValue)
        if (p1 == p2){
            if p1>2 {
                p1 -= 1
            }else{
                p1 += 1
            }
        }
        powerUp1?.texture = SKTexture(imageNamed: "powerup_\(p1)")
        powerUp2?.texture = SKTexture(imageNamed: "powerup_\(p2)")
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        let touch = touches.first!
        if nextLabel!.containsPoint(touch.locationInNode(self)) {
            if powerupPicked {
                toNextLevel()
            }
            
        }
        if mainMenuLabel!.containsPoint(touch.locationInNode(self)) {
            toMainMenu()
        }
        if saveLabel!.containsPoint(touch.locationInNode(self)) {
            save()
        }
        if powerUp1!.containsPoint(touch.locationInNode(self)) {
            runPowerup(p1)
        }
        if powerUp2!.containsPoint(touch.locationInNode(self)) {
            runPowerup(p2)
        }
    }
    
    func toNextLevel(){
        Music.sharedInstance.startMusic()
        let transferFade = SKAction.fadeInWithDuration(0.5)
        
        self.runAction(transferFade, completion: {
            let transition = SKTransition.doorsOpenHorizontalWithDuration(0.5)
            let nextLevelFileName : String = "Level\(self.nextLevel)"
            let swimScene = SwimScene(fileNamed: nextLevelFileName)
            swimScene?.currentLevel = self.nextLevel
            swimScene?.player = self.player
            self.view?.presentScene(swimScene!, transition: transition)
        })
    }
    
    func toMainMenu(){
        let transition = SKTransition.fadeWithColor(UIColor.blueColor(), duration: 1.2)
        let gameScene = GameScene(fileNamed: "GameScene")
        gameScene!.scaleMode = SKSceneScaleMode.AspectFill
        self.view?.presentScene(gameScene!, transition: transition)
    }
    
    func save(){
        print("SAVING")
        let savedLevel:Int = nextLevel
        NSUserDefaults.standardUserDefaults().setObject(savedLevel, forKey:"SavedGame")
        let life = player?.life
        NSUserDefaults.standardUserDefaults().setObject(life, forKey:"Life")
        let health = player?.health
        NSUserDefaults.standardUserDefaults().setObject(health, forKey:"Health")
        let maxSpeed = player?.maxSpeed
        NSUserDefaults.standardUserDefaults().setObject(maxSpeed, forKey:"MaxSpeed")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    override func update(currentTime: CFTimeInterval) {
        
    }
    
}
